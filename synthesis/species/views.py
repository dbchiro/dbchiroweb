from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class SpecieDistributionView(LoginRequiredMixin, TemplateView):
    template_name = "specie_distribution.html"
