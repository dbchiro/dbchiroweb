from rest_framework.serializers import BooleanField, IntegerField

from geodata.serializers import AreaGeomSerializer


class DistributionSerialiazer(AreaGeomSerializer):
    count_observations = IntegerField()
    count_sessions = IntegerField()
    count_places = IntegerField()
    count_taxa = IntegerField()
    last_data = IntegerField()
    breed_colo = BooleanField()
    has_gite = BooleanField()

    class Meta(AreaGeomSerializer.Meta):
        fields = AreaGeomSerializer.Meta.fields + (
            "count_observations",
            "count_sessions",
            "count_places",
            "count_taxa",
            "last_data",
            "breed_colo",
            "has_gite",
        )
