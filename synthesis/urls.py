from django.urls import path

from .areas.api import AreaSynthesisApi
from .areas.views import AreaView
from .contribs.views import GlobalContribView
from .species.api import SpecieDistribution
from .species.views import SpecieDistributionView

app_name = "synthesis"

urlpatterns = [
    # url de recherches de localités
    path("contrib", GlobalContribView.as_view(), name="global_contrib"),
    path("area/<int:pk>", AreaView.as_view(), name="area_synthesis"),
    path("api/v1/synthesis/area/<int:pk>", AreaSynthesisApi.as_view(), name="area_synthesis_api"),
    path("specie/distribution", SpecieDistributionView.as_view(), name="specie_distribution"),
    path(
        "api/v1/synthesis/specie/distribution",
        SpecieDistribution.as_view(),
        name="specie_distribution_api",
    ),
]
