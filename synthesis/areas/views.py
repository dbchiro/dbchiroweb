from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView

from geodata.models import Areas


class AreaView(LoginRequiredMixin, DetailView):
    model = Areas
    template_name = "areas.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-graph-trend"
        context["title"] = _("En synthèse")

        return context
