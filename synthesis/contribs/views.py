import arrow
from django.db.models import Q
from django.template.defaultfilters import date as trdate
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView

from accounts.models import Profile
from dicts.models import Contact
from geodata.models import Areas
from sights.models import Place, Session, Sighting


class GlobalContribView(TemplateView):
    template_name = "contribs.html"

    def __init__(self):
        self.user = None
        self.only_mine = None

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        self.user = request.user
        self.only_mine = (
            self.user.is_authenticated and request.GET.get("only_mine", None) is not None
        )

        context["icon"] = "fi-graph-trend"
        context["title"] = _("En synthèse")
        context["month_contrib_title"] = _("Observations au cours des 12 derniers mois")
        context["month_contrib"] = self.month_contrib()
        context["month_sights"] = self.month_sights()
        context["list_month"] = self.list_month()
        context["list_year"] = self.list_year()
        context["year_contrib_title"] = _("Nombre total de données par année")
        context["year_contrib"] = self.year_contrib()
        context["sight_date_label"] = _("par date d'observation")
        context["create_date_label"] = _("par date de saisie")
        context["year_sights"] = self.year_sights()
        context["year"] = arrow.now().year
        context["contact_contrib_title"] = _("Nombre total de données par type de contact")
        context["list_contact"] = self.list_contact()
        context["contact_contrib"] = self.contact_contrib()
        context["areas_contrib_title"] = _("Observations totales par zones")
        context["list_areas"] = self.list_areas()
        context["areas_contrib"] = self.areas_contrib()
        context["sightingsicon"] = "fi-eye"
        context["sightingscount"] = self.sighting_count()
        context["sightingstext"] = _("observations")
        context["placesicon"] = "fi-marker"
        context["placescount"] = self.place_count()
        context["placestext"] = _("localités")
        context["sessionsicon"] = "far fa-calendar-alt"
        context["sessionscount"] = self.sessions_count()
        context["sessionstext"] = _("sessions")
        context["observersicon"] = "fi-torsos-all-female"
        context["observerscount"] = self.observers_count()
        context["observerstext"] = _("observateurs")
        return self.render_to_response(context)

    def sighting_count(self):
        qs = Sighting.objects.all()
        if self.only_mine:
            qs = qs.filter(
                Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
            )
        return qs.count()

    def place_count(self):
        qs = Place.objects.all()
        if self.only_mine:
            qs = qs.filter(created_by=self.user)
        return qs.count()

    def sessions_count(self):
        qs = Session.objects.all()
        if self.only_mine:
            qs = qs.filter(Q(main_observer=self.user) | Q(other_observer=self.user))
        return qs.count()

    def observers_count(self):
        qs = Profile.objects.all()
        if self.only_mine:
            qs = qs.filter(created_by=self.user)
        return qs.count()

    def month_sights(self):
        final_data = []
        date = arrow.now()
        for month in range(12):
            date = date.shift(months=-1)
            qs = Sighting.objects.filter(
                session__date_start__gte=date.floor("month").datetime,
                session__date_start__lte=date.ceil("month").datetime,
            )
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data[::-1]

    def month_contrib(self):
        final_data = []
        date = arrow.now()
        for month in range(12):
            date = date.shift(months=-1)
            qs = Sighting.objects.filter(
                timestamp_create__gte=date.floor("month").datetime,
                timestamp_create__lte=date.ceil("month").datetime,
            )
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data[::-1]

    def list_month(self):
        date = arrow.now()
        final_data = [trdate(date.shift(months=-1), "M y") for m in range(12)]

        return final_data[::-1]

    def list_year(self):
        list_year = []
        last_year = arrow.now().year
        first_year = last_year - 10
        for year in range(first_year, last_year):
            year = year + 1
            list_year.append(year)
        return list_year

    def year_sights(self):
        final_data = []
        for year in self.list_year():
            date = arrow.get(year, 1, 1)
            # date = date.replace(years=-1)
            qs = Sighting.objects.filter(
                session__date_start__gte=date.floor("year").datetime,
                session__date_start__lte=date.ceil("year").datetime,
            )
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data

    def year_contrib(self):
        final_data = []
        for year in self.list_year():
            date = arrow.get(year, 1, 1)
            # date = date.replace(years=-1)
            qs = Sighting.objects.filter(
                timestamp_create__gte=date.floor("year").datetime,
                timestamp_create__lte=date.ceil("year").datetime,
            )
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data

    def list_contact(self):
        final_data = []
        for contact in Contact.objects.values_list("code", flat=True).distinct():
            final_data.append(contact)
        return final_data

    def contact_contrib(self):
        final_data = []
        for contact in Contact.objects.values_list("code", flat=True).distinct():
            qs = Sighting.objects.filter(session__contact__code=contact)
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data

    def list_areas(self):
        areas = (
            Areas.objects.filter(coverage=True)
            .values_list("name", flat=True)
            .distinct()
            .order_by("code")
        )
        return [area for area in areas]

    def areas_contrib(self):
        final_data = []
        areas = self.list_areas()
        for area in areas:
            qs = Sighting.objects.filter(session__place__areas__name=area)
            if self.only_mine:
                qs = qs.filter(
                    Q(session__main_observer=self.user) | Q(session__other_observer=self.user)
                )
            final_data.append(qs.count())
        return final_data
