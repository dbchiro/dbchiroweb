# import the logging library
import logging

import django_filters.rest_framework
from django.db.models import Prefetch, Q
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from core.mixins import SmallResultsSetPagination
from sights.models import Place, Session

from ..models import Study
from .permissions import StudyDetailPermission
from .serializers import StudyDataSerializer, StudySerializer

logger = logging.getLogger(__name__)


class StudyDatasApi(ListAPIView):
    serializer_class = StudyDataSerializer
    permission_classes = [StudyDetailPermission, IsAuthenticated]
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self, *args, **kwargs):
        # qs = super().get_queryset()7
        pk = self.kwargs.get("pk")
        qs = (
            Place.objects.select_related("created_by")
            .select_related("updated_by")
            .prefetch_related(
                Prefetch(
                    "sessions",
                    queryset=Session.objects.filter(study=pk),
                    to_attr="study_sessions",
                )
            )
            .prefetch_related("study_sessions__study")
            .prefetch_related(Prefetch("study_sessions__observations"))
            .prefetch_related(Prefetch("study_sessions__contact"))
            .prefetch_related(Prefetch("study_sessions__observations__codesp"))
            .prefetch_related(Prefetch("study_sessions__main_observer"))
            # .prefetch_related(Prefetch("sessions__other_observer"))
        )
        logger.debug(f"pk is {pk}")
        study = Study.objects.get(pk=pk)
        logger.debug(f"study is {study}")
        # user = self.request.user
        # if study.project_manager != self.request.user.pk
        qs = qs.filter(sessions__study=pk)
        return qs.distinct().order_by("-timestamp_update").all()


class StudyListApi(ListAPIView):
    serializer_class = StudySerializer
    queryset = (
        Study.objects.select_related("created_by")
        .select_related("updated_by")
        .select_related("project_manager")
        .prefetch_related("sessions")
        .order_by("timestamp_update")
        # .prefetch_related(Prefetch("sessions__other_observer"))
    )
    filter_backends = [filters.SearchFilter, django_filters.rest_framework.DjangoFilterBackend]
    pagination_class = SmallResultsSetPagination
    # filterset_fields = ["name__unaccent__lower__trigram_similar", "is_confidential"]
    filterset_fields = StudySerializer.Meta.fields
    search_fields = ["uuid", "name"]
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        user = self.request.user
        if user.access_all_data or user.edit_all_data or user.is_superuser:
            # If advanced user, can see all studies
            qs = qs
        if not (user.access_all_data or user.edit_all_data or user.is_superuser):
            # If not advanced user, can see own and public studies
            qs = qs.filter(
                (Q(confidential=True) & Q(project_manager=user))
                | (Q(confidential=True) & Q(created_by=user))
                | Q(confidential=False)
            )
        if self.request.query_params.get("id"):
            qs = qs.filter(id_study=self.request.query_params.get("id"))
        if self.request.query_params.get("search_name"):
            qs = qs.filter(
                name__unaccent__lower__trigram_similar=self.request.query_params.get("search_name")
            )
        return qs

    # TODO : Manage created_by checkbox
