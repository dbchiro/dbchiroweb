from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, TemplateView

from .models import Exports


class ExportDetail(LoginRequiredMixin, DetailView):
    template_name = "export.html"
    model = Exports

    def get_queryset(self):
        qs = super().get_queryset()
        qs.filter(created_by=self.request.user)
        return qs


class ExportList(LoginRequiredMixin, TemplateView):
    template_name = "export_list.html"
