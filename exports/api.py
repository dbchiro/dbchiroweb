from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from .models import Exports
from .serializers import ExportsSerializer


class ExportViewSet(
    LoginRequiredMixin,
    ViewSet,
):
    queryset = Exports.objects.all()
    serializer_class = ExportsSerializer

    """
    A simple ViewSet for listing or retrieving users.
    """

    def list(self, request):
        queryset = Exports.objects.filter(created_by=request.user).order_by("-timestamp_create")
        serializer = ExportsSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Exports.objects.filter(created_by=request.user)
        export = get_object_or_404(queryset, pk=pk)
        serializer = ExportsSerializer(export)
        return Response(serializer.data)
