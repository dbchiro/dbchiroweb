#!/bin/python3

"""
Accounts module - Tests
"""

from django.test import TestCase

from accounts.models import Profile


class ProfileTestCase(TestCase):
    def setUp(self):
        Profile.objects.create(
            username="jdoe",
            first_name="John",
            last_name="Doe",
            email="jdoe@test.com",
            is_staff=True,
            is_active=True,
        )
        Profile.objects.create(
            username="jbourne",
            first_name="Jason",
            last_name="Bourne",
            email="jbourne@test.com",
            is_active=False,
        )

    def test_user_exists(self):
        """Is users exists"""
        user_1 = Profile.objects.get(username="jdoe")
        user_2 = Profile.objects.get(username="jbourne")
        self.assertEqual(user_1.full_name(), "John DOE")
        self.assertEqual(user_2.full_name(), "Jason BOURNE")
        self.assertEqual(str(user_1), "jdoe")
        self.assertEqual(str(user_2), "jbourne")
        self.assertEqual(user_1.is_staff, True)
        self.assertEqual(user_2.is_staff, False)
        self.assertEqual(user_1.is_active, True)
        self.assertEqual(user_2.is_active, False)
