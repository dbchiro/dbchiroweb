# Generated by Django 3.2.13 on 2022-06-07 19:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0005_alter_profile_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="profile",
            name="export_all_data",
            field=models.BooleanField(
                default=False, verbose_name="Peut exporter toutes les données"
            ),
        ),
    ]
