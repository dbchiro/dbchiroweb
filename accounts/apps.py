#!/bin/python3

"""
Accounts module - App config
"""


from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = "accounts"
    verbose_name = "Gestion des comptes observateurs"
