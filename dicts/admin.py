import logging

from django.apps import apps
from django.contrib import admin
from django.core.exceptions import FieldDoesNotExist
from django.utils.translation import gettext_lazy as _

logger = logging.getLogger(__name__)


@admin.action(description=_("Mark selected items as active"))
def activate(_modeladmin, _request, queryset):
    """Set item active"""
    queryset.update(enabled=True)


@admin.action(description=_("Mark selected items as inactive"))
def inactivate(_modeladmin, _request, queryset):
    """Set item inactive"""
    queryset.update(enabled=False)


class ListAdminMixin(object):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields if field.name != "id"]
        super().__init__(model, admin_site)


def create_admin_class(model):
    # Create a new class dynamically
    class DynamicAdmin(ListAdminMixin, admin.ModelAdmin):
        pass

    # Check if the model has an "enabled" field
    if "enabled" in [field.name for field in model._meta.fields]:
        # If it has the "enabled" field, add the actions
        DynamicAdmin.actions = [activate, inactivate]
    try:
        model._meta.get_field("groupings")
        DynamicAdmin.filter_horizontal = ("groupings",)
    except FieldDoesNotExist:
        pass
    return DynamicAdmin


models = apps.get_app_config("dicts").get_models()
for model in models:
    admin_class = create_admin_class(model)
    try:
        admin.site.register(model, admin_class)
    except admin.sites.AlreadyRegistered as error:
        logger.error(error)
