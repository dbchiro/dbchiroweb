import json
import logging

from django.apps import apps
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.http import Http404, HttpResponse
from django.views.decorators.cache import cache_page

# Get an instance of a logger
logger = logging.getLogger(__name__)


CACHE_TTL = getattr(settings, "CACHE_TTL", DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def apiDicts(req):
    """List all values from all dicts in one single request"""

    dicts = {}
    app_models = apps.get_app_config("dicts").get_models()
    for model in app_models:
        name = model.__name__
        values = model.objects.all().values()
        list_values = [entry for entry in values]
        dicts[name] = list_values

    jsondicts = json.dumps(dicts)

    return HttpResponse(jsondicts, content_type="application/json")


@cache_page(CACHE_TTL)
def apiDict(req, model):
    """List all values from all dicts in one single request"""

    try:
        model = apps.get_model(".".join(["dicts", model]))
        logger.debug(f"MODEL {model}")
        values = model.objects.all().values()
        list_values = [entry for entry in values]
        jsondicts = json.dumps(list_values)
        return HttpResponse(jsondicts, content_type="application/json")
    except Exception as e:
        logger.error(f"ERROR {e}")
        raise Http404
        # return HttpResponse({"error": e}, status=500, content_type="application/json")
