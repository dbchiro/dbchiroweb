from rest_framework.serializers import ModelSerializer


class DictsGenericSerializer(ModelSerializer):
    class Meta:
        fields = ("id", "code", "descr")
        abstract = True
