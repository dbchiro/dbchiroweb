from django.urls import path

from dicts.api import apiDict, apiDicts

app_name = "dicts"

urlpatterns = [
    # url de recherches de localités
    path("api/v1/dicts", apiDicts, name="api_dicts"),
    path("api/v1/dict/<str:model>", apiDict, name="api_dict_by_model"),
]
