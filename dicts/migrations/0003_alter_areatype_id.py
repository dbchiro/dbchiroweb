# Generated by Django 3.2.13 on 2022-05-05 19:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("dicts", "0002_auto_20211201_2227"),
    ]

    operations = [
        migrations.AlterField(
            model_name="areatype",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
