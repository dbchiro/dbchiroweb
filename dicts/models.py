from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from .managers import DictsBaseManager, SpeciesManager

# Create your models here.


class DictBaseModel(models.Model):
    code = models.CharField(max_length=20, unique=True, blank=False, null=False, default="-")
    objects = DictsBaseManager()

    def natural_key(self):
        """Natural primary key"""
        return (self.code,)

    class Meta:
        abstract = True


class MetaplaceType(DictBaseModel):
    """
    Types méta-localité utilisé dans la relation :model:`sights.Metaplace`.
    """

    category = models.CharField(max_length=30)
    descr = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.category} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Types de méta-localités"
        ordering = ["category", "code"]


class TypePlace(DictBaseModel):
    """
    Types localité utilisé dans la relation :model:`sights.Place`.
    """

    category = models.CharField(max_length=30)
    descr = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.code} ∙ {self.category} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Types de localités"
        ordering = ["category", "code"]


class PlacePrecision(DictBaseModel):
    """
    Précisions géographiques utilisé dans la relation :model:`sights.Place`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Types de précisions des localités"


class State(DictBaseModel):
    """
    Etat des gîtes
    """

    def __str__(self):
        return f"{self.code}"

    class Meta:
        verbose_name_plural = "Etat des gîtes"


class Interest(DictBaseModel):
    """
    Intérêt des gîtes
    """

    def __str__(self):
        return f"{self.code}"

    class Meta:
        verbose_name_plural = "Intérêt des gîtes"


class PlaceBatiDetail(DictBaseModel):
    """
    Méthodes d'inventaire/d'observations :model:`sights.Sighting`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Type de bâti"


class GiteBatAccess(models.Model):
    """
    Accessibilité des éléments des gites aux chiros
    """

    descr = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Accessibilité des éléments d'un gîte"


class BuildCover(models.Model):
    """
    Accessibilité des éléments des gites aux chiros
    """

    descr = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = _("Types de couvertures d'un bâtiment")


class TreeSpecies(models.Model):
    """
    Dionnaire des essences d'arbres
    Pour les bases françaises, l'id de la table correspond au champ cd_nom du référentiel taxonomique TaxRef
    """

    common_name = models.CharField(max_length=200)
    latin_name = models.CharField(max_length=200)
    full_latin_name = models.CharField(max_length=300)
    tax_level = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.full_latin_name}"

    class Meta:
        verbose_name_plural = _("Espèces d'arbres")


class TreeHealth(models.Model):
    """
    Etats de santé de l'arbre
    """

    descr = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = _("Etats de santé de l'arbre")


class TreeContext(DictBaseModel):
    """
    Tree biotop context :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Contextes écologiques des arbres"


class TreeForestStands(DictBaseModel):
    """
    Forest stands :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Peuplements forestiers"


class TreeCircumstance(DictBaseModel):
    """
    Discover tree circumstances :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Circonstances de la découverte de l'arbre"


class TreeGiteType(DictBaseModel):
    """
    Tree gite type :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Types de gites arboricoles"


class TreeGiteOrigin(DictBaseModel):
    """
    Tree gite origin :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Origines des types de gîtes arboricoles"


class TreeGitePlace(DictBaseModel):
    """
    Tree gite place :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Localisations des gîtes arboricoles"


class TreeBecoming(DictBaseModel):
    """
    Tree becoming :model:`sights.PlaceTreeDetail`.
    """

    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Devenirs des arbres"


class LinkType(DictBaseModel):
    """Types d'hyperliens"""

    label = models.CharField(max_length=50, verbose_name=_("Description"))

    def __str__(self):
        return f"{self.label}"

    class Meta:
        verbose_name_plural = "Types de hyperliens"


class Contact(DictBaseModel):
    """Types de contacts"""

    descr = models.CharField(max_length=50, verbose_name=_("Contact"))

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Types de contacts"


class Method(DictBaseModel):
    """
    Méthodes d'inventaire/d'observations :model:`sights.Sighting`.
    """

    contact = models.CharField(max_length=5, verbose_name="Type de contact")
    descr = models.CharField(max_length=50, verbose_name=_("Description"))

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Méthodes d'inventaire/d'observations"


class Specie(models.Model):
    sys_order = models.IntegerField()
    codesp = models.CharField(max_length=20, unique=True)
    sp_true = models.BooleanField(default=False)
    sci_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    common_name_fr = models.CharField(max_length=255, blank=True, null=True)
    common_name_eng = models.CharField(max_length=255, blank=True, null=True)
    groupings = models.ManyToManyField("self", limit_choices_to={"sp_true": True})
    enabled = models.BooleanField(default=True, verbose_name=_("Actif"))

    objects = SpeciesManager()

    def __str__(self):
        return f"{self.codesp} ∙ {self.common_name_fr}"

    def natural_key(self):
        """Natural primary key"""
        return (self.codesp,)

    class Meta:
        verbose_name_plural = "Liste des codes espèces de la base de données"
        ordering = ["sp_true", "sys_order"]


class SpecieDetail(models.Model):
    specie = models.OneToOneField(Specie, on_delete=models.CASCADE)
    sys_order = models.IntegerField()
    id_n2000 = models.IntegerField(blank=True, null=True)
    id_faunaeuropea = models.IntegerField(blank=True, null=True)
    id_cdref_taxref = models.IntegerField(blank=True, null=True)
    id_inpn2008 = models.IntegerField(blank=True, null=True)
    id_visionature = models.IntegerField(blank=True, null=True)
    id_serena = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return models.Model.__str__(self)

    class Meta:
        verbose_name_plural = (
            "Données détaillées des espèces (correspondances de référentiels, etc.)"
        )


class SpecieStatus(models.Model):
    specie = models.OneToOneField(Specie, on_delete=models.CASCADE)
    protnat = models.CharField(max_length=10, blank=True, null=True)
    dhff = models.CharField(max_length=10, blank=True, null=True)
    bern_conv = models.CharField(max_length=10, blank=True, null=True)
    bonn_conv = models.CharField(max_length=10, blank=True, null=True)
    bonn_conv_eurobats = models.CharField(max_length=10, blank=True, null=True)
    world_redlist = models.CharField(max_length=10, blank=True, null=True)
    medit_redlist = models.CharField(max_length=10, blank=True, null=True)
    europ_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_nat_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_regra_redlist = models.CharField(max_length=10, blank=True, null=True)
    fr_regauv_redlist = models.CharField(max_length=10, blank=True, null=True)

    def __str__(self):
        return models.Model.__str__(self)

    class Meta:
        verbose_name_plural = "Statuts réglementaires des espèces"


class PropertyDomain(DictBaseModel):

    domain = models.CharField(max_length=15)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Types de propriétés foncières"


class CountPrecision(DictBaseModel):
    contact = models.CharField(max_length=5, verbose_name=_("type de contacts"), default="0")
    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Précisions de comptages"


class TypeDevice(DictBaseModel):
    contact = models.CharField(max_length=2)
    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.descr}"

    class Meta:
        verbose_name_plural = "Types de dispositifs d'échantillonnage"


class CountUnit(DictBaseModel):
    contact = models.CharField(max_length=5, verbose_name=_("type de contacts"), default="0")
    descr = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Unités de comptages"


class Sex(DictBaseModel):
    """Sexes"""

    descr = models.CharField(max_length=50, verbose_name=_("Sexe"))

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Sexes"


class Age(DictBaseModel):
    descr = models.CharField(max_length=100, verbose_name=_("Age"))

    def __str__(self):
        return f"{self.code} ∙ {self.descr}"

    class Meta:
        verbose_name_plural = "Ages"


class Period(DictBaseModel):
    """Périodes"""

    descr = models.CharField(max_length=50, verbose_name=_("Période"))
    period = models.CharField(max_length=50)
    day_start = models.IntegerField()
    month_start = models.IntegerField()
    day_end = models.IntegerField()
    month_end = models.IntegerField()

    def __str__(self):
        return "%s ∙ %s du %s/%s au %s/%s" % (
            self.code,
            self.descr,
            self.day_start,
            self.month_start,
            self.day_end,
            self.month_end,
        )

    class Meta:
        verbose_name_plural = "Périodes du cycle annuel"
        ordering = [
            "id",
        ]


class BiomTesticule(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Testicule"))
    descr = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat des testicules"


class BiomEpipidyme(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Epididyme"))
    descr = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat des épididymes"


class BiomTuniqVag(DictBaseModel):

    short_descr = models.CharField(max_length=50)
    descr = models.CharField(max_length=100, verbose_name=_("Tunique vaginale"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat des tuniques vaginales"


class BiomGlandTaille(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Taille des glandes"))
    descr = models.CharField(max_length=100, verbose_name=_("Taille des glandes"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Taille des glandes buccales"


class BiomGlandCoul(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Couleur des glandes"))
    descr = models.CharField(max_length=100, verbose_name=_("Couleur des glandes"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Couleurs des glandes buccales"


class BiomMamelle(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Mamelles"))
    descr = models.CharField(max_length=250, verbose_name=_("Mamelles"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat des mamelles"


class BiomGestation(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Gestation"))
    descr = models.CharField(max_length=100, verbose_name=_("Gestation"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat de la gestation"


class BiomEpiphyse(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Epiphyse"))
    descr = models.CharField(max_length=100, verbose_name=_("Epididyme"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat des épiphyses articulaires"


class BiomChinspot(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Tâche mentonière"))
    descr = models.CharField(max_length=100, verbose_name=_("Tâche mentonière"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = "Etat du chinspot"


class BiomDent(DictBaseModel):

    short_descr = models.CharField(max_length=50, verbose_name=_("Usure des dents"))
    descr = models.CharField(max_length=100, verbose_name=_("Usure des dents"))

    def __str__(self):
        return f"{self.id} ∙ {self.code} ∙ {self.short_descr}"

    class Meta:
        verbose_name_plural = _("Etat de l'usure des dents")


class PlaceManagementAction(DictBaseModel):

    category = models.CharField(max_length=100, verbose_name=_("Catégorie d'action de gestion"))
    label = models.CharField(max_length=250, verbose_name=_("Action de gestion"))

    def __str__(self):
        return f"{self.label}"

    class Meta:
        verbose_name = _("Actions de gestion")


class LandCoverCLC(models.Model):
    code_lev3 = models.CharField(
        max_length=3, primary_key=True, verbose_name=_("Code Corine Land Cover (niv3)")
    )
    label_lev3 = models.CharField(
        max_length=150, verbose_name=_("Libelle Corine Land Cover (niv3)")
    )
    code_lev2 = models.CharField(max_length=3, verbose_name=_("Code Corine Land Cover (niv2)"))
    label_lev2 = models.CharField(
        max_length=150, verbose_name=_("Libelle Corine Land Cover (niv2)")
    )
    code_lev1 = models.CharField(max_length=3, verbose_name=_("Code Corine Land Cover (niv1)"))
    label_lev1 = models.CharField(
        max_length=150, verbose_name=_("Libelle Corine Land Cover (niv1)")
    )

    def __str__(self):
        return f"{self.code_lev3} ∙ {self.label_lev3}"

    class Meta:
        verbose_name = _("Correspondance des codes CorineLandCover")


class AreaType(models.Model):
    code = models.CharField(max_length=50, verbose_name=_("Code"), null=False, blank=False)
    name = models.CharField(max_length=250, verbose_name=_("Nom"), null=False, blank=False)
    source = models.TextField(null=True, blank=True)
    descr = models.TextField(null=True, blank=True)

    def __str__(self):
        return f"{self.code} - {self.name}"

    class Meta:
        verbose_name = _("Types de zonages")
