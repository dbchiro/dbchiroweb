"""
    Vues de l'application Sights
"""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from sights.forms import MetaplaceForm
from sights.mixins import PlaceEditAuthMixin
from sights.models import Metaplace, Place
from sights.tables import PlaceTable

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class MetaplaceCreate(LoginRequiredMixin, CreateView):
    """Create view for Metaplace model"""

    model = Metaplace
    form_class = MetaplaceForm
    template_name = "leaflet_form.html"

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Création d'un métasite"
        context[
            "js"
        ] = """

        """
        return context


class MetaplaceUpdate(PlaceEditAuthMixin, UpdateView):
    """UpdateView form Metaplace"""

    model = Metaplace
    form_class = MetaplaceForm
    template_name = "leaflet_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = "Modification d'un métasite"
        context[
            "js"
        ] = """
        """
        return context


class MetaplaceDelete(PlaceEditAuthMixin, DeleteView):
    model = Metaplace
    template_name = "confirm_delete.html"
    success_url = reverse_lazy("blog:home")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'un métasite")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer ce métasite")
        return context


class MetaplaceList(LoginRequiredMixin, TemplateView):
    template_name = "metaplace_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Rechercher un méta site")
        context["createmetaplacebtn"] = True
        context[
            "js"
        ] = """
        """
        return context


class MetaplaceDetail(LoginRequiredMixin, DetailView):
    model = Metaplace
    template_name = "metaplace_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        placeobjects = (
            Place.objects.filter(metaplace_id=pk)
            .select_related("created_by", "updated_by", "type")
            .prefetch_related("sessions")
        )
        placecount = placeobjects.count()
        placetable = PlaceTable(placeobjects)

        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Détail d'un métasite")
        context[
            "js"
        ] = """
        """
        context["placeicon"] = "fas fa-map-marker-alt"
        context["placetitle"] = _("Localités")
        context["placecount"] = placecount
        context["placetable"] = placetable
        return context
