import django_filters
from dal import autocomplete
from django.utils.translation import gettext_lazy as _

from dicts.models import MetaplaceType, TypePlace
from sights.forms import MetaplaceSearchFilterForm, PlaceSearchFilterForm
from sights.models import Place


class PlaceFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label=_("Nom de la localité"), lookup_expr="icontains")
    areas = django_filters.CharFilter(
        label=_("Zonages"),
        widget=autocomplete.ListSelect2(
            url="api:areas_autocomplete", attrs={"class": "listselect2"}
        ),
    )
    type = django_filters.ModelChoiceFilter(
        label=_("Type de localité"), queryset=TypePlace.objects.all()
    )
    metaplace = django_filters.CharFilter(
        label=_("Métasite"),
        widget=autocomplete.ListSelect2(
            url="api:metaplace_autocomplete", attrs={"class": "listselect2"}
        ),
    )
    metaplace_type = django_filters.ModelChoiceFilter(
        label=_("Type de métasite"), queryset=MetaplaceType.objects.all()
    )

    class Meta:
        model = Place
        fields = ["name", "areas", "type", "metaplace"]
        order_by = ["name"]
        form = PlaceSearchFilterForm


class MetaplaceFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label=_("Nom du métasite"), lookup_expr="icontains")
    type = django_filters.ModelChoiceFilter(
        label=_("Type de métasite"), queryset=MetaplaceType.objects.all()
    )

    class Meta:
        model = Place
        fields = ["name", "type"]
        order_by = ["name"]
        form = MetaplaceSearchFilterForm
