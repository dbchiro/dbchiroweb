"""
    Vues de l'application Sights
"""

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_tables2 import SingleTableView

from core import js
from geodata.models import Areas
from sights.forms import DeviceForm, SessionChangePlaceForm, SessionForm, SessionLinkForm
from sights.mixins import SessionEditAuthMixin, SessionViewAuthMixin, SightingEditAuthMixin
from sights.models import Device, Session, SessionLink, Sighting
from sights.tables import SessionDeviceTable, SessionSightingTable, SessionTable

IMAGE_FILE_TYPES = ["png", "jpg", "jpeg"]
DOCUMENT_FILE_TYPES = ["doc", "docx", "odt", "pdf"]


class SessionCreate(LoginRequiredMixin, CreateView):
    model = Session
    form_class = SessionForm
    template_name = "normal_form.html"

    def get_initial(self):
        initial = super().get_initial()
        initial = initial.copy()
        initial["main_observer"] = self.request.user
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        place_id = self.kwargs.get("pk")
        kwargs["place_id"] = place_id
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get("pk")
        try:
            return super().form_valid(form)
        except IntegrityError as e:
            messages.error(self.request, e.__cause__)
            return HttpResponseRedirect(self.request.path)
        # except IntegrityError as e:
        # # messages.error(self.request, e.__cause__)
        # messages.error(self.request, _('Cette session existe déjà'))
        # return HttpResponseRedirect(self.request.path)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-map"
        context["title"] = _("Ajout d'une session")
        # context['js'] = """
        # $(function () {
        #     $('.dateinput').fdatepicker({
        #         format: 'dd/mm/yyyy',
        #         disableDblClickSelection: true,
        #         leftArrow: '<i class="fa fa-fw fa-chevron-left"></i>',
        #         rightArrow: '<i class="fa fa-fw fa-chevron-right"></i>',
        #     });
        # });
        # """
        context["js"] = js.DateAndTimeInput
        return context

    # Improve workflow - Go to observation
    def get_success_url(self):
        id_session = self.object.id_session
        if self.request.method == "POST" and "gotoObs" in self.request.POST:
            return reverse_lazy("sights:sighting_edit_form", kwargs={"pk": id_session})
        return reverse_lazy("sights:session_detail", kwargs={"pk": id_session})


class SessionUpdate(SessionEditAuthMixin, UpdateView):
    model = Session
    form_class = SessionForm
    file_storage = FileSystemStorage()
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        place_id = self.kwargs.get("pk")
        kwargs["place_id"] = place_id
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "far fa-calendar-alt"
        context["title"] = _("Modification d'une session")
        context["js"] = js.DateAndTimeInput
        return context


class SessionChangePlaceUpdate(SessionEditAuthMixin, UpdateView):
    model = Session
    form_class = SessionChangePlaceForm
    file_storage = FileSystemStorage()
    template_name = "normal_form.html"

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-exclamation-triangle"
        context["title"] = _("Changer la localité de la session")
        context[
            "js"
        ] = """
        """
        return context


class SessionDelete(SessionEditAuthMixin, DeleteView):
    model = Session
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:place_detail", kwargs={"pk": self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une session")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer la session")
        return context


class SessionDetail(SessionViewAuthMixin, DetailView):
    model = Session
    template_name = "session_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get("pk")
        # Rendu des sessions
        context["icon"] = "far fa-calendar-alt"
        context["title"] = _("Détails de la session d'inventaire")
        context[
            "js"
        ] = """
        """
        # Rendu des observations
        context["sightingicon"] = "fa fa-eye fa-fw"
        context["sightingtitle"] = _("Observations")
        context["sightingcount"] = Sighting.objects.filter(session=pk).distinct().count()
        context["sightingtable"] = SessionSightingTable(
            Sighting.objects.filter(session=pk).distinct().order_by("-timestamp_update")
        )
        # Rendu des dispositifs
        context["deviceicon"] = "fi-target"
        context["devicetitle"] = _("Dispositifs d'échantillonnage")
        context["devicecount"] = Device.objects.filter(session=pk).count()
        context["devicetable"] = SessionDeviceTable(Device.objects.filter(session=pk))
        return context


class SessionMyList(LoginRequiredMixin, SingleTableView):
    table_class = SessionTable
    template_name = "table.html"
    table_pagination = {"per_page": 25}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        loggeduser = self.request.user
        context["icon"] = "far fa-calendar-alt"
        context["title"] = _("Mes sessions")
        context[
            "js"
        ] = """
        """
        context["counttitle"] = _("Nombre de sessions")
        context["count"] = (
            Session.objects.filter(
                Q(created_by=loggeduser)
                | Q(main_observer=loggeduser)
                | Q(other_observer__username__contains=loggeduser.username)
            )
            .distinct()
            .count()
        )
        return context

    def get_queryset(self):
        loggeduser = self.request.user
        queryset = (
            Session.objects.select_related("created_by")
            .select_related("main_observer")
            .prefetch_related("other_observer")
            .select_related("place")
            .select_related("contact")
            .select_related("study")
            .prefetch_related("observations")
            .prefetch_related("observations__codesp")
            .prefetch_related("place__areas")
            .prefetch_related("place__type")
            .prefetch_related("place__type")
        )
        queryset = (
            queryset.filter(
                Q(created_by=loggeduser)
                | Q(main_observer=loggeduser)
                | Q(other_observer__username__contains=loggeduser.username)
            )
            .distinct()
            .order_by("-timestamp_update")
        )
        return queryset


class DeviceCreate(LoginRequiredMixin, CreateView):
    model = Device
    form_class = DeviceForm
    template_name = "normal_form.html"

    # def get(self, request, *args, **kwargs):
    #     session = Session.objects.get(id_session=self.kwargs.get('pk'))
    #     self.form.fields['type'].queryset = TypeDevice.objects.filter(contact=session.contact.code)
    #     return super().get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        session_id = self.kwargs.get("pk")
        kwargs["contact"] = Session.objects.get(id_session=session_id).contact.code
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.session_id = self.kwargs.get("pk")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-target-two"
        context["title"] = _("Ajout d'un dispositif")
        context[
            "js"
        ] = """
        """
        return context


class DeviceUpdate(SightingEditAuthMixin, UpdateView):
    model = Device
    form_class = DeviceForm
    template_name = "normal_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        session_id = self.object.session_id
        kwargs["contact"] = Session.objects.get(id_session=session_id).contact.code
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-target-two"
        context["title"] = _("Modification d'un dispositif")
        context[
            "js"
        ] = """
        """
        return context


class DeviceDelete(SightingEditAuthMixin, DeleteView):
    model = Device
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:session_detail", kwargs={"pk": self.object.session_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une dispositif d'échantillonage")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer le dispositif")
        return context


class SessionLinkCreate(LoginRequiredMixin, CreateView):
    model = SessionLink
    form_class = SessionLinkForm
    template_name = "normal_form.html"

    # def get(self, request, *args, **kwargs):
    #     session = Session.objects.get(id_session=self.kwargs.get('pk'))
    #     self.form.fields['type'].queryset = TypeDevice.objects.filter(contact=session.contact.code)
    #     return super().get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.session_id = self.kwargs.get("pk")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-link"
        context["title"] = _("Ajout d'un lien")
        context["js"] = ""
        return context


class SessionLinkUpdate(SightingEditAuthMixin, UpdateView):
    model = SessionLink
    form_class = SessionLinkForm
    template_name = "normal_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fi-link"
        context["title"] = _("Modification d'un lien")
        context["js"] = ""
        return context


class SessionLinkDelete(SightingEditAuthMixin, DeleteView):
    model = SessionLink
    template_name = "confirm_delete.html"

    def get_success_url(self):
        return reverse_lazy("sights:session_detail", kwargs={"pk": self.object.session_id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["icon"] = "fa fa-fw fa-trash"
        context["title"] = _("Suppression d'une lien")
        context["message_alert"] = _("Êtes-vous certain de vouloir supprimer le lien")
        return context


class SessionSearch(LoginRequiredMixin, TemplateView):
    template_name = "session_search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        enable_areas = Areas.objects.filter(coverage=True).all()
        context["enable_areas"] = enable_areas
        context["icon"] = "fa fa-fw fa-calendar-alt"
        context["title"] = _("Rechercher une session")
        context[
            "js"
        ] = """
        """
        return context
