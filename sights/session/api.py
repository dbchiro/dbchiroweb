from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from rest_framework.generics import ListAPIView

from core.mixins import SmallResultsSetPagination

from ..mixins import SessionFilteringMixin, SessionListPermissionsMixin
from ..models import Session, Sighting
from .serializers import SessionGeoSerializer, SessionSerializer


def get_obs_queryset(user):
    queryset = (
        Sighting.objects.select_related("session")
        .select_related("session__main_observer")
        .prefetch_related("session__other_observer")
    )

    if user.access_all_data or user.edit_all_data or user.is_superuser:
        return queryset
    if settings.SEE_ALL_NON_SENSITIVE_DATA:
        if user.is_resp:
            queryset = (
                queryset.filter(
                    Q(
                        Q(created_by=user)
                        | Q(observer=user)
                        | Q(session__main_observer=user)
                        | Q(session__other_observer=user)
                        | Q(session__study__project_manager=user)
                    )
                    | Q(session__place__areas__in=user.resp_areas.all())
                    | Q(
                        ~Q(session__place__areas__in=user.resp_areas.all())
                        & (
                            (Q(session__is_confidential=False))
                            & (
                                Q(session__place__is_hidden=False)
                                | Q(session__place__authorized_user=user)
                            )
                        )
                    )
                )
                .distinct()
                .order_by("-timestamp_update")
            )
        else:
            queryset = (
                queryset.filter(
                    (
                        Q(created_by=user)
                        | Q(observer=user)
                        | Q(session__main_observer=user)
                        | Q(session__other_observer=user)
                        | Q(session__study__project_manager=user)
                    )
                    | (
                        (Q(session__is_confidential=False))
                        & (
                            Q(session__place__is_hidden=False)
                            | Q(session__place__authorized_user=user)
                        )
                    )
                )
                .distinct()
                .order_by("-timestamp_update")
            )
    else:
        if user.is_resp:
            queryset = (
                queryset.filter(
                    Q(
                        Q(created_by=user)
                        | Q(observer=user)
                        | Q(session__main_observer=user)
                        | Q(session__other_observer=user)
                        | Q(session__study__project_manager=user)
                    )
                    | Q(session__place__areas__in=user.resp_areas.all())
                )
                .distinct()
                .order_by("-timestamp_update")
            )
        else:
            queryset = (
                queryset.filter(
                    Q(created_by=user)
                    | Q(observer=user)
                    | Q(session__other_observer__username__contains=user.username)
                    | Q(session__study__project_manager=user)
                )
                .distinct()
                .order_by("-timestamp_update")
            )

    return queryset


session_queryset = (
    Session.objects.select_related("contact")
    .select_related("place")
    .prefetch_related("place__areas")
    .prefetch_related("place__areas__area_type")
    .select_related("main_observer", "study")
    .prefetch_related("observations__codesp")
    .prefetch_related("other_observer")
    .order_by("-date_start")
)


class SessionApi(
    LoginRequiredMixin,
    SessionListPermissionsMixin,
    SessionFilteringMixin,
    ListAPIView,
):
    queryset = session_queryset.distinct()
    serializer_class = SessionSerializer
    pagination_class = SmallResultsSetPagination

    def get_serializer_class(self):
        geojson = self.request.query_params.get("geojson", None)
        if geojson:
            return SessionGeoSerializer
        return self.serializer_class
