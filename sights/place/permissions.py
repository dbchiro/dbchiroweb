import logging

from rest_framework.permissions import BasePermission
from sinp_nomenclatures.models import Nomenclature

logger = logging.getLogger(__name__)

logger.debug(f"type BasePermission {type(BasePermission)}")


class PlaceActorPermissions(BasePermission):
    """Sighting Edit API permissions

    :param BasePermission: [description]
    :type BasePermission: [type]
    """

    def has_permission(self, request, view) -> bool:
        """[summary]

        :param request: [description]
        :type request: [type]
        :param view: [description]
        :type view: [type]
        :return: [description]
        :rtype: [type]
        """
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj) -> bool:
        user = request.user
        user_organism_role = user.organismmember_set.first()
        # Deny actions on objects if the user is not authenticated
        if not user.is_authenticated:
            return False
        if view.action in ["update", "partial_update", "destroy"]:
            return (
                user == obj.created_by
                or user.is_superuser
                or user.is_staff
                or user.edit_all_data
                or user == obj.natural_person
                or (
                    user_organism_role
                    and (
                        user_organism_role.organim == obj.legal_person
                        and Nomenclature.objects.get(type__code="8", code="manager")
                        in user_organism_role.member_level.all()
                    )
                )
            )
        else:
            return True
