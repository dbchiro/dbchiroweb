# Generated by Django 4.2.5 on 2024-01-23 13:19

from django.db import migrations, models


def set_metaplace_areas(apps, schema_editor):
    Metaplace = apps.get_model("sights", "Metaplace")
    Areas = apps.get_model("geodata", "Areas")
    for metaplace in Metaplace.objects.all():
        if metaplace.geom:
            metaplace.areas.set(Areas.objects.filter(geom__intersects=metaplace.geom).all())
            metaplace.save()


def unset_metaplace_areas(apps, schema_editor):
    Metaplace = apps.get_model("sights", "Metaplace")
    for metaplace in Metaplace.objects.all():
        metaplace.areas.set([])
        metaplace.save()


class Migration(migrations.Migration):
    dependencies = [
        ("geodata", "0004_auto_20220505_2143"),
        ("sights", "0007_alter_place_x_alter_place_y_alter_sighting_period"),
    ]

    operations = [
        migrations.AddField(
            model_name="metaplace",
            name="areas",
            field=models.ManyToManyField(
                blank=True,
                db_index=True,
                related_name="metaplaces",
                to="geodata.areas",
                verbose_name="Zonages",
            ),
        ),
        migrations.RunPython(set_metaplace_areas, unset_metaplace_areas),
    ]
