# Generated by Django 4.2.16 on 2024-11-19 10:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("sights", "0016_alter_session_main_observer_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="place",
            name="is_hidden",
            field=models.BooleanField(
                db_index=True, default=False, verbose_name="Localité confidentielle"
            ),
        ),
        migrations.AlterField(
            model_name="session",
            name="is_confidential",
            field=models.BooleanField(default=False, verbose_name="Session confidentielle"),
        ),
    ]
