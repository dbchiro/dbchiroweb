-- SET SEARCH_PATH TO src_dbchirogcra,public;

INSERT INTO dicts_method (id, code, contact, descr)
VALUES (27, 'duns', 'du', 'Détection acoustique (méthode non spécifiée)'),
       (28, 'vmns', 'vm', 'Vu en main (méthode non spécifiée)'),
       (29, 'tens', 'te', 'Télémétrie (méthode non spécifiée)'),
       (30, 'vvns', 'vv', 'Vu en visuel (méthode non spécifiée)'),
       (31, 'ncns', 'nc', 'Non communiqué (méthode non spécifiée)')
ON CONFLICT DO NOTHING;
BEGIN;

-- count data with missing details;
SELECT count(DISTINCT sights_sighting.id_sighting) FILTER (WHERE sights_countdetail IS NULL) no_details
FROM sights_sighting
         LEFT JOIN sights_countdetail ON sights_sighting.id_sighting = sights_countdetail.sighting_id;


DROP VIEW IF EXISTS src_dbchirogcra.v_sights_sighting_detail_status CASCADE;
CREATE VIEW src_dbchirogcra.v_sights_sighting_detail_status AS
(
SELECT sights_sighting.id_sighting
     , dicts_contact.code
     , sights_countdetail.id_countdetail IS NULL                   AS no_detail
     , sights_sighting.total_count
     , sights_sighting.total_count - sum(sights_countdetail.count) AS detail_count_less_total_count
     , array_agg(sights_countdetail.count)                         AS agg_count
     , array_agg(sights_countdetail.id_countdetail)                AS agg_id_countdetail
     , count(id_countdetail)                                       AS nb_countdetail
FROM sights_sighting
         JOIN sights_session ON sights_sighting.session_id = sights_session.id_session
         JOIN dicts_contact ON sights_session.contact_id = dicts_contact.id
         LEFT JOIN sights_countdetail ON sights_sighting.id_sighting = sights_countdetail.sighting_id

GROUP BY sights_sighting.id_sighting, total_count, sights_countdetail.id_countdetail IS NULL,
         dicts_contact.code
HAVING NOT (sights_sighting.total_count - sum(sights_countdetail.count) = 0 AND count(id_countdetail) > 0)
    );


DROP VIEW IF EXISTS src_dbchirogcra.v_sights_sighting_details_status_recap;
CREATE VIEW src_dbchirogcra.v_sights_sighting_details_status_recap AS
SELECT code,
       count(*),
       CASE
           WHEN detail_count_less_total_count > 0 THEN 'total_count > sum details'
           WHEN detail_count_less_total_count < 0 THEN 'total_count > sum details'
           WHEN detail_count_less_total_count = 0 THEN 'total_count = sum details'
           END               AS diff,
       nb_countdetail > 0    AS has_details,
       array_agg(session_id) AS sessions
FROM src_dbchirogcra.v_sights_sighting_detail_status
         JOIN sights_sighting ON v_sights_sighting_detail_status.id_sighting = sights_sighting.id_sighting
GROUP BY code,
         CASE
             WHEN detail_count_less_total_count > 0 THEN 'total_count > sum details'
             WHEN detail_count_less_total_count < 0 THEN 'total_count > sum details'
             WHEN detail_count_less_total_count = 0 THEN 'total_count = sum details'
             END,
         nb_countdetail > 0
ORDER BY code, diff;

SELECT *
FROM src_dbchirogcra.v_sights_sighting_details_status_recap;

BEGIN;

-- VV data
w
INSERT INTO sights_countdetail (uuid, count, comment, timestamp_create,
                                timestamp_update, created_by_id,
                                method_id, precision_id, unit_id, sighting_id,
                                updated_by_id)
SELECT uuid_generate_v4(),
       coalesce(sights_sighting.total_count, 1),
       'Donnée détaillée générée en base de donnée le 2023-12-19',
       now(),
       now(),
       sights_sighting.created_by_id,
       dicts_method.id,
       dicts_countprecision.id,
       dicts_countunit.id,
       sights_sighting.id_sighting,
       NULL
FROM sights_sighting
         JOIN v_sights_sighting_detail_status
              ON sights_sighting.id_sighting = v_sights_sighting_detail_status.id_sighting
        ,
     dicts_countprecision,
     dicts_countunit,
     dicts_method
WHERE NOT (v_sights_sighting_detail_status.nb_countdetail > 0 AND detail_count_less_total_count IS NULL)
  AND v_sights_sighting_detail_status.code LIKE 'vv'
  AND dicts_method.code LIKE 'vvns'
  AND dicts_countunit.code = 'ind'
  AND dicts_countprecision.code = 'precis'
;

COMMIT;

-- TODO: correctly populate old teletric data with transmitters references
-- BEGIN;
-- -- te data
--
-- INSERT INTO sights_countdetail (uuid, count, comment, timestamp_create,
--                                 timestamp_update, created_by_id,
--                                 method_id, precision_id, unit_id, sighting_id,
--                                 updated_by_id)
-- SELECT uuid_generate_v4(),
--        coalesce(sights_sighting.total_count, 1) AS count,
--        'Donnée détaillée générée en base de donnée le 2023-12-19',
--        now(),
--        now(),
--        sights_sighting.created_by_id,
--        dicts_method.id,
--        dicts_countprecision.id,
--        dicts_countunit.id,
--        sights_sighting.id_sighting,
--        NULL
-- FROM sights_sighting
--          JOIN v_sights_sighting_detail_status
--               ON sights_sighting.id_sighting = v_sights_sighting_detail_status.id_sighting
--         ,
--      dicts_countprecision,
--      dicts_countunit,
--      dicts_method
-- WHERE NOT (v_sights_sighting_detail_status.nb_countdetail > 0 AND detail_count_less_total_count IS NULL)
--   AND v_sights_sighting_detail_status.code LIKE 'te'
--   AND dicts_method.code LIKE 'tens'
--   AND dicts_countunit.code = 'azi'
--   AND dicts_countprecision.code = 'telem'
-- ;
--
-- SELECT *
-- FROM dicts_countprecision;
--
-- SELECT v_sights_sighting_detail_status.total_count,
--        count(*),
--        array_agg(v_sights_sighting_detail_status.id_sighting),
--        array_agg(session_id) AS sessions
-- FROM v_sights_sighting_detail_status
--          JOIN sights_sighting ON v_sights_sighting_detail_status.id_sighting = sights_sighting.id_sighting
-- WHERE code LIKE 'te'
-- GROUP BY v_sights_sighting_detail_status.total_count;
-- ROLLBACK ;
BEGIN;
-- Manage
BEGIN;
INSERT INTO sights_countdetail (uuid, count, comment, timestamp_create,
                                timestamp_update, created_by_id,
                                method_id, precision_id, unit_id, sighting_id,
                                updated_by_id)
SELECT uuid_generate_v4(),
       coalesce(sights_sighting.total_count, 1) AS count,
       'Donnée détaillée générée en base de donnée le 2023-12-19',
       now(),
       now(),
       sights_sighting.created_by_id,
       dicts_method.id,
       dicts_countprecision.id,
       dicts_countunit.id,
       sights_sighting.id_sighting,
       NULL
FROM sights_sighting
         JOIN v_sights_sighting_detail_status
              ON sights_sighting.id_sighting = v_sights_sighting_detail_status.id_sighting
        ,
     dicts_countprecision,
     dicts_countunit,
     dicts_method
WHERE NOT (v_sights_sighting_detail_status.nb_countdetail > 0 AND detail_count_less_total_count IS NULL)
  AND v_sights_sighting_detail_status.code LIKE 'vm'
  AND dicts_method.code LIKE 'vmns'
  AND dicts_countunit.code = 'ind'
  AND dicts_countprecision.code = 'precis'
;
COMMIT;


SELECT total_count, count(*), array_agg(id_sighting), array_agg(session_id)
FROM sights_sighting
         JOIN sights_session ON sights_sighting.session_id = sights_session.id_session
         JOIN dicts_contact ON sights_session.contact_id = dicts_contact.id
WHERE code = 'vm'
  AND total_count > 700
GROUP BY 1;


BEGIN;
--NC data
INSERT INTO sights_countdetail (uuid, count, comment, timestamp_create,
                                timestamp_update, created_by_id,
                                method_id, precision_id, unit_id, sighting_id,
                                updated_by_id)
SELECT uuid_generate_v4(),
       coalesce(sights_sighting.total_count, 1) AS count,
       'Donnée détaillée générée en base de donnée le 2023-12-19',
       now(),
       now(),
       sights_sighting.created_by_id,
       dicts_method.id,
       NULL,
       NULL,
       sights_sighting.id_sighting,
       NULL
FROM sights_sighting
         JOIN v_sights_sighting_detail_status
              ON sights_sighting.id_sighting = v_sights_sighting_detail_status.id_sighting
        ,
     dicts_countprecision,
     dicts_countunit,
     dicts_method
WHERE NOT (v_sights_sighting_detail_status.nb_countdetail > 0 AND detail_count_less_total_count IS NULL)
  AND v_sights_sighting_detail_status.code LIKE 'nc'
  AND dicts_method.code LIKE 'ncns'
  AND dicts_countunit.code = 'ind'
  AND dicts_countprecision.code = 'precis'
;
COMMIT;


ROLLBACK;
BEGIN;
--DU data
INSERT INTO sights_countdetail (uuid, count, comment, timestamp_create,
                                timestamp_update, created_by_id,
                                method_id, precision_id, unit_id, sighting_id,
                                updated_by_id)
SELECT uuid_generate_v4(),
       1 AS count,
       'Donnée détaillée générée en base de donnée le 2023-12-19',
       now(),
       now(),
       sights_sighting.created_by_id,
       dicts_method.id,
       NULL,
       NULL,
       sights_sighting.id_sighting,
       NULL
FROM sights_sighting
         JOIN v_sights_sighting_detail_status
              ON sights_sighting.id_sighting = v_sights_sighting_detail_status.id_sighting
        ,
     dicts_countprecision,
     dicts_countunit,
     dicts_method
WHERE NOT (v_sights_sighting_detail_status.nb_countdetail > 0 AND detail_count_less_total_count IS NULL)
  AND v_sights_sighting_detail_status.code LIKE 'du'
  AND dicts_method.code LIKE 'duns'
  AND dicts_countunit.code = 'jourp'
  AND dicts_countprecision.code = 'acoust'
;

COMMIT;


SELECT *
FROM sights_sighting
WHERE id_sighting = 151851;


SELECT id_sighting, session_id, array_agg(sights_countdetail.comment), count(*)
FROM sights_sighting
         JOIN sights_countdetail ON sights_sighting.id_sighting = sights_countdetail.sighting_id
GROUP BY 1, 2
HAVING count(*) > 1
   AND string_agg(sights_countdetail.comment, ',') LIKE '%Donnée détaillée générée en base de donnée le 2023-12-19%';

SELECT *
FROM src_dbchirogcra.dicts_countprecision;

SELECT count, total_count, id_countdetail
FROM sights_countdetail
         JOIN sights_sighting ON sights_countdetail.sighting_id = sights_sighting.id_sighting
         JOIN sights_session ON sights_sighting.session_id = sights_session.id_session
where id_session =60179;

select * from sights_countdetail where id_countdetail=65574;

select * from sights_sighting where id_sighting=137393;
