(classic-install)=

## Installation "classique"

L'installation de [dbChiro\[web\]](http://dbchiro.org) a été uniquement testée sur serveur Debian et Ubuntu.

**Prerequis**:

- Un serveur Debian 64 bits
- Un émulateur de terminal pour la connexion via ssh (natif sous Linux, [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/) ou [mobaXterm](https://mobaxterm.mobatek.net/) sous windows)

### Prérequis

Les applications suivants doivent être installées sur le serveur:

* Python3, python3-pip, libgdal et python-poetry (python package manager)
* Base de donnée PostgreSQL/PostGIS
* Proxy web Apache2


```bash
sudo apt install python3 python-is-python3 postgresql-13 postgresql-13-postgis-3 python3 python3-pip libgdal-dev git apache2
```

### Procédure d'installation recommandée

L'installation peut se faire soit en téléchargeant la dernière version officielle depuis le dépot (branche master sur https://framagit.org/dbchiro/dbchiroweb/), soit en clonant le dépot via `git` (Méthode recommandée pour faciliter les mises à jour).

Dans cet exemple, dbchiroweb sera installé dans le dossier `/var/www/dbchiro`

```sh
mkdir /var/www/dbchiro
cd /var/www/dbchiro
git clone https://framagit.org/dbchiro/dbchiroweb.git
```

### Création de l'environnement virtuel

```bash
cd /var/www/dbchiro/dbchiroweb
python3 -m venv venv
source venv/bin/activate
pip install poetry
poetry install
```

### Création de la base de donnée et du superutilisateur de la base de donnée

```bash
# en tant que root
su postgres
psql
```

```sql
CREATE DATABASE dbchirodb;
CREATE ROLE dbchiropguser LOGIN SUPERUSER ENCRYPTED PASSWORD 'motdepasse';
ALTER DATABASE dbchirodb OWNER TO dbchiropguser;
-- Se connecter à la base de donnée avec la commande \c
\c dbchirodb
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
CREATE EXTENSION "uuid-ossp";
-- Quitter psql
\q
```

### Configuration de dbChiroWeb

Copiez le fichier `.env.sample` en le renommant `.env` dans ce même dossier:

```bash
cd /var/www/dbchiro/dbchiroweb
cp .env.sample .env
editor .env
```

```{literalinclude} ../../.env.sample
:language: ini
```

D'autres paramétrages sont tout à fait possible, cf. le fichier de configuration
Python avec toutes les variables déclarées dans les fonctions `config("MA_VAR", ...)`.


```{literalinclude} ../../dbchiro/settings.py
:language: python
```

**La répartition des périodes est la suivante**


```{eval-rst}
+-----------------------------------------+----------+-------------+-----------+----------+
|Jour de l'année                          | 335      | 60          | 136       | 228      |
+-----------------------------------------+----------+-------------+-----------+----------+
|date                                     | 01/12    | 01/03       | 16/05     | 16/08    |
+=========================================+==========+=============+===========+==========+
|Hivernage / wintering (w)                |     ≥    |     <       |           |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit printanier / sping transit (st)  |          |     ≥       |     <     |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Estivage / Summering (e)                 |          |             |     ≥     |    <     |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit automnal / Automn transit (ta)   |     <    |             |     ≥     |          |
+-----------------------------------------+----------+-------------+-----------+----------+
```

### Initialisation de la base de données

```sh
cd /var/www/dbchiro/dbchiroweb
source ../venv/bin/activate
# Création des fichiers d'initialisation de la base de donnée
python -m manage migrate
# Création du dossier 'static' et importation des fichiers nécessaire au fonctionnement de l'interface graphique (css, js, polices).
python manage.py collectstatic
# Création du premier utilisateur, administrateur du site
python manage.py createsuperuser
# Peuplement des données de base
python -m manage loaddata dicts/fixtures/dicts.xml
python -m manage loaddata geodata/fixtures/dep.xml.gz
python -m manage loaddata geodata/fixtures/mun.xml.gz
python -m manage loaddata sinp_dict_data_v1.0.json
# Lancement du serveur
python manage.py runserver
```

Le terminal devrait alors renvoyer le résultat suivant:

```text
...
Django version 4.2.11, using settings 'dbchiro.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

#### Personalisation du logo

Un logo du bandeau supérieur du site est fourni dans `core/static/img/logo_site.png`. Pour l'installer, il suffit d'éxécuter la commande suivante mais il est tout à fait possible de le remplacer par un autre image, au format `png` qui aura le même nom :

```sh
cd /var/www/dbchiro/dbchiroweb
cp core/static/img/logo_site.png.sample media/logo_site.png
```

### Configuration du serveur apache

Crééez le fichier de configuration du serveur apache. Cela par du principe que le domaine, ou sous-domaine, dispose d'une redirection vers l'adresse IP du présent serveur.
Plus d'informations à cette adresse: <https://docs.djangoproject.com/fr/1.11/howto/deployment/wsgi/modwsgi/>

```sh
# Activation du module wsgi
a2enmod wsgi
# Copiez l'exemple de fichier de configuration apache pour dbChiro dans le dossier des sites disponibles d'apache et adaptez le à votre installation.
cd /var/www/dbchiro/dbchiroweb
cp docker/apache_dbchiro.conf.sample /etc/apache2/sites-available/dbchiro.conf
```

Voici à quoi il ressemble:

```apache
# Il est important de conserver WSGIDaemonProcess en dehors de <VirtualHost ...></VirtualHost> pour l'utilisation de letsencrypt
WSGIDaemonProcess dbchiro python-home=/var/www/dbchiro/venv python-path=/var/www/dbchiro/dbchiroweb
<VirtualHost moninstance.dbchiro.org:80>
    ServerName moninstance.dbchiro.org
    ServerAdmin admin@dbchiro.org
    Alias /static /var/www/dbchiro/dbchiroweb/dbchiro/static
    WSGIProcessGroup dbchiro
    WSGIScriptAlias / /var/www/dbchiro/dbchiroweb/dbchiro/wsgi.py
    <Directory /var/www/dbchiro/dbchiroweb/dbchiro>
        <files wsgi.py>
            Require all granted
        </files>
    </Directory>
    CustomLog /var/www/dbchiro/custom.log combined
    ErrorLog /var/www/dbchiro/error.log
</VirtualHost>
```

Activez cette configuration avec la commande suivante:

```sh
a2ensite dbchiro.conf
service apache2 restart
```

### Sécurisation HTTPS

Il est vivement recommandé de paramétrer le https sur le site, cela peut se faire avec [let's encrypt](https://letsencrypt.org/).

Vous devriez maintenant accéder à votre plateforme depuis le nom de domaine renseigné.

Connectez vous avec votre compte superuser précédemment créé.
