# import the logging library
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from core.mixins import SmallResultsSetPagination

from .models import Areas
from .serializers import AreasSimpleSerializer

# from rest_framework.views import APIView


logger = logging.getLogger(__name__)


class AreaListApi(
    LoginRequiredMixin,
    ListAPIView,
):
    serializer_class = AreasSimpleSerializer
    queryset = Areas.objects.all().select_related("area_type").all()
    pagination_class = SmallResultsSetPagination
    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
    )
    filterset_fields = [
        "id",
        "name",
        "code",
        "area_type",
    ]
    search_fields = ["name", "code"]
    permission_classes = [
        IsAuthenticated,
    ]
