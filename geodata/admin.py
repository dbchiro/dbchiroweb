from django.contrib.gis import admin
from django.utils.translation import gettext_lazy as _

from .models import Areas, LandCover


@admin.action(description=_("Marque les territoires sélectionnés comme couverture territoriale"))
def make_coverage(modeladmin, request, queryset):
    queryset.update(coverage=True)


@admin.action(description=_("Démarque les territoires sélectionnés comme couverture territoriale"))
def make_not_coverage(modeladmin, request, queryset):
    queryset.update(coverage=False)


class AreasAdmin(admin.GeoModelAdmin):
    list_display = [
        "id",
        "area_type",
        "code",
        "name",
        "coverage",
    ]
    search_fields = [
        "area_type",
        "code",
        "name",
        "coverage",
    ]
    list_filter = (
        "area_type",
        "coverage",
    )
    actions = [make_coverage, make_not_coverage]


admin.site.register(Areas, AreasAdmin)
admin.site.register(LandCover, admin.GeoModelAdmin)
