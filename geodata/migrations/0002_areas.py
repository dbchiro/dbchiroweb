# Generated by Django 2.2.24 on 2021-12-01 21:27

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("geodata", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="Areas",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
                    ),
                ),
                ("name", models.CharField(db_index=True, max_length=250, verbose_name="Nom")),
                ("code", models.CharField(db_index=True, max_length=100, verbose_name="Code")),
                (
                    "coverage",
                    models.BooleanField(
                        db_index=True, default=False, verbose_name="Territoire couvert"
                    ),
                ),
                (
                    "geom",
                    django.contrib.gis.db.models.fields.MultiPolygonField(
                        srid=4326, verbose_name="Emprise géographique"
                    ),
                ),
            ],
            options={
                "verbose_name": "Zonage géographique",
                "verbose_name_plural": "Zonages géographiques",
            },
        ),
    ]
