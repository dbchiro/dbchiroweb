from django.urls import path

from .views import SystemInformation, UnauthorizedModify, UnauthorizedView, version_api

app_name = "core"
urlpatterns = [
    # url d'erreurs
    path("unauthorizedmodify", UnauthorizedModify.as_view(), name="update_unauth"),
    path("unauthorizedview", UnauthorizedView.as_view(), name="view_unauth"),
    # Info systeme
    path("sysinfo", SystemInformation.as_view(), name="sysinfo"),
    path("version", version_api, name="version"),
]
