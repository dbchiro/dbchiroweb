import json

from django.conf import settings

from core.functions import version


def site_info(request):
    """Return SITE NAME to frontend"""
    return {
        "SITE_NAME": settings.SITE_NAME,
        "VERSION": version(),
        "SEE_ALL_NON_SENSITIVE_DATA": settings.SEE_ALL_NON_SENSITIVE_DATA,
        "MAP_TILES": json.dumps(settings.TILES),
        "DEFAULT_CENTER": json.dumps(settings.LEAFLET_CONFIG["DEFAULT_CENTER"]),
        "PERIODS": json.dumps(
            [
                settings.PERIOD_WINTERING_VALUE,
                settings.PERIOD_SPRING_TRANSIT_VALUE,
                settings.PERIOD_SUMMERING_VALUE,
                settings.PERIOD_AUTUMN_TRANSIT_VALUE,
            ]
        ),
    }
