"""
    Mixins
"""

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import redirect
from django.urls import reverse
from rest_framework.pagination import PageNumberPagination


class ManageAccountAuthMixin:
    """
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    """

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect("%s?next=%s" % (reverse("auth_login"), request.path))
        else:
            loggeduser = request.user
            if (
                loggeduser.edit_all_data
                or loggeduser.access_all_data
                or loggeduser.is_resp
                or loggeduser.is_staff
                or loggeduser.is_superuser
            ):
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("core:view_unauth")


class AdminRequiredMixin(LoginRequiredMixin, UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = "page_size"
    max_page_size = 10000


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = "page_size"
    max_page_size = 1000


class SmallResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 100
